import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: colors.purple.darken4,
    accent: colors.grey.darken3,
    secondary: colors.purple.lighten1,
    info: colors.blue,
    warning: colors.orange.darken1,
    error: colors.red.accent2,
    success: colors.green
  }
})
